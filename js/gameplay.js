const GAME = {
    _startTime: 0,
    _playerName: null,
    _coins: 100,
    _playTimeSimulator: null,

    set startTime(startTime){
        this._startTime = startTime;
        console.log("starting game on " + startTime.toLocaleDateString() + " at " + startTime.toLocaleTimeString());
    },
    get startTime(){
        return this._startTime;
    },

    set playerName(playerName){
        this._playerName = playerName;
        console.log("player name is " + this._playerName);
        document.getElementById("playerName").innerHTML = this.playerName;

    },
    get playerName(){
        return this._playerName;
    },

    set coins(amount){
        this._coins = amount;
        document.getElementById("coins").innerHTML = this.coins.toFixed(1);
    },
    get coins(){
        return this._coins;
    },

    calculatePlayTime(){
        if(this._playTimeSimulator !== null){
            clearInterval(this._playTimeSimulator);
        }
        this._playTimeSimulator = setInterval(
            function () {
                let d = new Date();
                let playTime = d-GAME.startTime;
                document.getElementById("playTime").innerHTML = Math.round(playTime/1000);
            }, 1000
        );
    },
};

let RUBBERBANDS;
RUBBERBANDS = {
    _made: 0,
    _available: 0,
    _sold: 0,
    _price: 1,
    _demand: 1,
    _demandSimulator: null,

    set made(amount) {
        this._made = amount;
        document.getElementById("rubberbandsMade").innerHTML = this.made.toFixed();
    },
    get made() {
        return this._made;
    },

    set available(amount) {
        this._available = amount;
        document.getElementById("rubberbandsAvailable").innerHTML = this.available.toFixed();
    },
    get available() {
        return this._available;
    },

    set sold(amount) {
        this._sold = amount;
        document.getElementById("rubberbandsSold").innerHTML = this.sold.toFixed();
    },
    get sold() {
        return this._sold;
    },

    set price(price) {
        this._price = price;
        document.getElementById("rubberbandsPrice").innerHTML = this.price.toFixed(1);
        this.simulateDemand();
    },
    get price() {
        return this._price
    },

    set demand(factor) {
        this._demand = factor;
        document.getElementById("rubberbandsDemand").innerHTML = (this.demand.toFixed() * 100).toString();
        this.simulateDemand();
    },
    get demand() {
        return this._demand;
    },

    calculateDemandPerSec() {
        let base = this.price;
        let exponent = -0.75;
        let calculatedDemandPerSec = 0 - this.price + 5 * this.demand;
        if (calculatedDemandPerSec < 0.0001) {
            return 0.0001;
        }
        return calculatedDemandPerSec;
    },

    make(amount) {
        let rubberNeeded = amount / RUBBER.efficiency;
        if (RUBBER.use(rubberNeeded) !== false) {
            this.made += amount;
            this.available += amount;
        } else {
            return false;
        }
    },

    sell(amount) {
        if (this.available >= amount) {
            this.available -= amount;
            this.sold += amount;
            GAME.coins += this.price * amount;
        } else {
            GAME.coins += this.price * this.available;
            this.sold += this.available;
            this.available = 0;
        }
    },

    simulateDemand() {
        let demand = this.calculateDemandPerSec();
        let frequency = 1000;
        if(demand > 100) {
            demand *= 10;
            frequency = 100;
        } else {
            frequency = 1000/demand;
        }
        if (this._demandSimulator !== null) {
            clearInterval(this._demandSimulator);
        }
        this._demandSimulator = setInterval(function () {
            RUBBERBANDS.sell(demand)
        }, frequency);
    },

    raisePrice(amount) {
        this.price += amount;
    },

    lowerPrice(amount) {
        if (this.price - amount >= 0) {
            this.price -= amount;
        }
    }
};


const RUBBER = {
    _amount: 1000,
    _efficiency: 1,
    _price : 20,
    _priceAvg: 20,
    _priceFluctuationSimulator: null,

    set amount(amount){
        this._amount = amount;
        document.getElementById("rubberAmount").innerHTML = this.amount.toFixed();
    },
    get amount(){
        return this._amount;
    },

    set efficiency(factor){
        this._efficiency = factor;
        document.getElementById("rubberEfficiency").innerHTML = (this.efficiency.toFixed()*100).toString();
    },
    get efficiency(){
        return this._efficiency;
    },

    set price(price){
        this._price = price;
        document.getElementById("rubberPrice").innerHTML = this.price.toFixed();
    },
    get price(){
        return this._price
    },

    set priceAvg(avg){
        this._priceAvg = price;
    },
    get priceAvg(){
        return this._priceAvg
    },

    simulatePriceFluctuation(){
        if(this._priceFluctuationSimulator !== null){
            clearInterval(this._demandSimulator);
        }
        this._priceFluctuationSimulator = setInterval(function() {
            RUBBER.calculatePrice();
        }, 5000);
    },

    calculatePrice(){
        let min = Math.ceil(this.priceAvg - (this.priceAvg * 0.2));
        let max = Math.floor(this.priceAvg + (this.priceAvg * 0.2));
        this.price = Math.floor(Math.random() * (max - min)) + min;
    },

    buy(amount) {
        let coinsNeeded = this.price/1000*amount;
        if(GAME.coins >= coinsNeeded) {
            GAME.coins -= coinsNeeded;
            this.amount += amount;
            return amount;
        } else {
            return false;
        }

    },

    use(amount){
        if(this.amount >= amount && this.amount !== 0) {
            this.amount -= amount;
            return amount;
        } else {
            return false;
        }
    }
};

const MACHINES = {
    _amount: 0,
    _efficiency: 1,
    _price: 100,
    _productionSimulator: null,

    set amount(amount){
        this._amount = amount;
        document.getElementById("machinesAmount").innerHTML = this.amount.toFixed();
    },
    get amount(){
        return this._amount;
    },

    set efficiency(perSec){
        this._efficiency = perSec;
        document.getElementById("machinesEfficiency").innerHTML = this.efficiency.toFixed();
        this.simulateProduction(this.amount*this.efficiency);
    },
    get efficiency(){
        return this._efficiency;
    },

    set price(price){
        this._price = price;
        document.getElementById("machinesPrice").innerHTML = this.price.toFixed();
    },
    get price(){
        return this._price
    },

    calculatePrice(amount){
        let calculatedPrice = this.price*((Math.pow(1.5,amount)-1)/(1.5-1));
        return calculatedPrice;
    },

    buy(amount){
        let coinsNeeded = this.calculatePrice(amount);
        if(GAME.coins >= coinsNeeded){
            this.amount += amount;
            this.price = this.calculatePrice(amount+1)-this.price;
            GAME.coins -= coinsNeeded;
            this.simulateProduction(this.amount*this.efficiency);
        } else {
            return false;
        }
    },

    simulateProduction(perSec){
        if(this._productionSimulator !== null){
            clearInterval(this._demandSimulator);
        }
        this._productionSimulator = setInterval(function() {
            RUBBERBANDS.make(perSec);
        }, 1000);
    },

};

const CAMPAIGNS = {
    local: {cost:2000, effect:2, element:"campaignsLocal", started:false},
    regional: {cost:40000, effect:4, element:"campaignsRegional", started:false},
    national: {cost:800000, effect:8, element:"campaignsNational", started:false},
    continental: {cost:16000000, effect:16, element:"campaignsContinental", started:false},
    global: {cost:320000000, effect:32, element:"campaignsGlobal", started:false},

    init(){
        document.querySelector("#" + this.local.element + " .cost").innerHTML = this.local.cost;
        document.querySelector("#" + this.regional.element + " .cost").innerHTML = this.regional.cost;
        document.querySelector("#" + this.national.element + " .cost").innerHTML = this.national.cost;
        document.querySelector("#" + this.continental.element + " .cost").innerHTML = this.continental.cost;
        document.querySelector("#" + this.global.element + " .cost").innerHTML = this.global.cost;
    },

    startCampaign(campaign){
        let coinsNeeded = campaign.cost;
        if(GAME.coins >= coinsNeeded && campaign.started === false){
            GAME.coins -= coinsNeeded;
            campaign.started = true;
            RUBBERBANDS.demand = campaign.effect;
            document.getElementById(campaign.element).style.display = 'none';
        } else {
            return false;
        }
    }
};

const RESEARCH = {
    ARB: {cost:1000, effect:"this.automateRubberBuyer()", element:"researchARB", researched:false},
    REF1: {cost:1000, effect:"RUBBER.efficiency = 2", element:"researchREF1", researched:false},
    MEF1: {cost:20000, effect:"MACHINES.efficiency = 4", element:"researchMEF1", researched:false},
    MEF2: {cost:400000, effect:"MACHINES.efficiency = 8", element:"researchMEF2", researched:false},
    MEF3: {cost:8000000, effect:"MACHINES.efficiency = 16", element:"researchMEF3", researched:false},

    init(){
        document.querySelector("#" + this.ARB.element + " .cost").innerHTML = this.ARB.cost;
        document.querySelector("#" + this.REF1.element + " .cost").innerHTML = this.REF1.cost;
        document.querySelector("#" + this.REF1.element + " .cost").innerHTML = this.REF1.cost;
        document.querySelector("#" + this.MEF2.element + " .cost").innerHTML = this.MEF2.cost;
        document.querySelector("#" + this.MEF3.element + " .cost").innerHTML = this.MEF3.cost;
    },

    doResearch(research){
        let coinsNeeded = research.cost;
        if(GAME.coins >= coinsNeeded && research.researched === false){
            GAME.coins -= coinsNeeded;
            research.researched = true;
            eval(research.effect);
            document.getElementById(research.element).style.display = 'none';
        } else {
            return false;
        }
    },

    automateRubberBuyer(){
        setInterval(function() {
            if(RUBBER.amount < 1000){
                RUBBER.buy(1000);
            }
        }, 10);
    }
};





function startGame(){
    let playerForm = document.getElementById("playerForm")
    let playerName = document.getElementById("playerNameInput").value;
    if (playerForm.style.display === "none") {
        playerForm.style.display = "block";
    } else {
        playerForm.style.display = "none";
        document.getElementById("game").style.display = "block";
    }

    if(playerName !== ""){
        GAME.playerName = playerName;
    } else {
        GAME.playerName = "Tycoon"
    }

    GAME.startTime = new Date();
    GAME.calculatePlayTime();
    GAME.coins = 1000000;
    RUBBERBANDS.price = 1;
    RUBBERBANDS.simulateDemand(1);
    RUBBER.simulatePriceFluctuation();
    MACHINES.price = 100;
    CAMPAIGNS.init();
    RESEARCH.init();
}


